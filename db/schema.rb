# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171027174836) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "casts", force: :cascade do |t|
    t.integer "movie_id", null: false
    t.integer "person_id", null: false
    t.integer "character_name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "name", null: false
    t.string "iso", null: false
  end

  create_table "crews", force: :cascade do |t|
    t.integer "movie_id", null: false
    t.integer "person_id", null: false
    t.integer "department_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "external_links", force: :cascade do |t|
    t.string "linkable_type"
    t.bigint "linkable_id"
    t.string "imdb_id"
    t.string "tmdb_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["linkable_type", "linkable_id"], name: "index_external_links_on_linkable_type_and_linkable_id"
  end

  create_table "genders", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "genres", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "genres_movies", id: false, force: :cascade do |t|
    t.bigint "genre_id", null: false
    t.bigint "movie_id", null: false
  end

  create_table "languages", force: :cascade do |t|
    t.string "name", null: false
    t.string "iso", null: false
  end

  create_table "media", force: :cascade do |t|
    t.string "mediumable_type"
    t.bigint "mediumable_id"
    t.integer "type_id", null: false
    t.string "path", null: false
    t.index ["mediumable_type", "mediumable_id"], name: "index_media_on_mediumable_type_and_mediumable_id"
  end

  create_table "movie_users", force: :cascade do |t|
    t.integer "movie_id", null: false
    t.integer "user_id", null: false
    t.integer "rating"
    t.boolean "favourite_ind"
    t.boolean "watchlist_ind"
    t.boolean "watched_ind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "movies", force: :cascade do |t|
    t.string "title", null: false
    t.integer "country_id", null: false
    t.integer "language_id", null: false
    t.integer "status_id"
    t.date "release_date"
    t.text "summary", null: false
    t.integer "runtime"
    t.boolean "adult_movie_ind"
    t.string "budget"
    t.string "revenue"
    t.string "original_title"
    t.string "homepage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "movies_production_companies", id: false, force: :cascade do |t|
    t.bigint "movie_id", null: false
    t.bigint "production_company_id", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string "name", null: false
    t.integer "gender_id"
    t.date "birth_date"
    t.date "death_date"
    t.string "place_of_birth"
    t.string "biography"
  end

  create_table "production_companies", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "types", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
