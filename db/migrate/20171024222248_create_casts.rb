class CreateCasts < ActiveRecord::Migration[5.1]
  def change
    create_table :casts do |t|
      t.column :movie_id, :integer, null: false
      t.column :person_id, :integer, null: false
      t.column :character_name, :integer, null: false
      t.timestamps
    end
  end
end
