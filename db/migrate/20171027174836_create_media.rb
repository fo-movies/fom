class CreateMedia < ActiveRecord::Migration[5.1]
  def change
    create_table :media do |t|
      t.references :mediumable, polymorphic: true, index: true
      t.column :type_id, :integer, null: false
      t.column :path, :string, null: false
    end
  end
end
