class CreateTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :types do |t|
      t.column :name, :string, null: false
    end
  end
end
