class ExternalLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :external_links do |t|
      t.references :linkable, polymorphic: true, index: true
      t.column :imdb_id, :string
      t.column :tmdb_id, :string
      t.timestamps
    end
  end
end
