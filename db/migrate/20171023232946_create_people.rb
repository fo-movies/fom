class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.column :name, :string, null: false
      t.column :gender_id, :integer
      t.column :birth_date, :date
      t.column :death_date, :date
      t.column :place_of_birth, :string
      t.column :biography, :string
    end
  end
end
