class CreateJoinTableMoviesProductionCompanies < ActiveRecord::Migration[5.1]
  def change
    create_join_table :movies, :production_companies do |t|
    end
  end
end
