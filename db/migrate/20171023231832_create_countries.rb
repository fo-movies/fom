class CreateCountries < ActiveRecord::Migration[5.1]
  def change
    create_table :countries do |t|
      t.column :name, :string, null: false
      t.column :iso, :string, null: false
    end
  end
end
