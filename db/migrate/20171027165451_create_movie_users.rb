class CreateMovieUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :movie_users do |t|
      t.column :movie_id, :integer, null: false
      t.column :user_id, :integer, null: false
      t.column :rating, :integer
      t.column :favourite_ind, :boolean
      t.column :watchlist_ind, :boolean
      t.column :watched_ind, :boolean
      t.timestamps
    end
  end
end
