class CreateCrews < ActiveRecord::Migration[5.1]
  def change
    create_table :crews do |t|
      t.column :movie_id, :integer, null: false
      t.column :person_id, :integer, null: false
      t.column :department_id, :integer, null: false
      t.timestamps
    end
  end
end
