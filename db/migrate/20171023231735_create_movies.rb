class CreateMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :movies do |t|
      t.column :title, :string, null: false
      t.column :country_id, :integer, null: false
      t.column :language_id, :integer, null: false
      t.column :status_id, :integer
      t.column :release_date, :date
      t.column :summary, :text, null: false
      t.column :runtime, :integer
      t.column :adult_movie_ind, :boolean, default: false
      t.column :budget, :decimal, precision: 23, scale: 5
      t.column :revenue, :decimal, precision: 23, scale: 5
      t.column :original_title, :string
      t.column :homepage, :string
      t.timestamps
    end
  end
end
