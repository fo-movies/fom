class CreateProductionCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :production_companies do |t|
      t.column :name, :string, null: false
    end
  end
end
