module Services
  class BusinessService

    attr_reader :user

    def initialize(user: nil, engine: nil)
      @user = user
      @engine = engine
    end

    def engine
      @engine ||= Storage::Allocator.new
    end

    def movie_builder
      @movie_builder ||= builder.movie_builder
    end

    def movie_storage
      @movie_store ||= engine.movie_store
    end

    def ref_data_storage
      @ref_data_store ||= engine.reference_data_store
    end

    private

    def builder
      @builder ||= ::Builders::ObjectBuilder.new(user: user)
    end
  end
end