module Services
  class MovieService < BusinessService
    def new_movie
      movie_builder.new_movie
    end
  end
end