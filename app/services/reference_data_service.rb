module Services
  class ReferenceDataService < BusinessService
    def get_countries
      ref_data_storage.get_countries
    end

    def get_languages
      ref_data_storage.get_languages
    end
  end
end