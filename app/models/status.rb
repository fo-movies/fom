# == Schema Information
#
# Table name: statuses
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Status < ApplicationRecord

  validates :name, presence: true, uniqueness: true
end
