# == Schema Information
#
# Table name: movie_users
#
#  id            :integer          not null, primary key
#  movie_id      :integer          not null
#  user_id       :integer          not null
#  rating        :integer
#  favourite_ind :boolean
#  watchlist_ind :boolean
#  watched_ind   :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class MovieUser < ApplicationRecord

end
