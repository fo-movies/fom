# == Schema Information
#
# Table name: departments
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Department < ApplicationRecord

  validates :name, presence: true, uniqueness: true
end
