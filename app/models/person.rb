# == Schema Information
#
# Table name: people
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  gender_id      :integer
#  birth_date     :date
#  death_date     :date
#  place_of_birth :string
#  biography      :string
#

class Person < ApplicationRecord
  has_many :media, as: :mediumable
  has_many :external_links, as: :linkable
end
