# == Schema Information
#
# Table name: genders
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Gender < ApplicationRecord

  validates :name, presence: true, uniqueness: true
end
