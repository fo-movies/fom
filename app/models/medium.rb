# == Schema Information
#
# Table name: media
#
#  id              :integer          not null, primary key
#  mediumable_type :string
#  mediumable_id   :integer
#  type_id         :integer          not null
#  path            :string           not null
#

class Medium < ApplicationRecord
  belongs_to :mediumable, polymorphic:  true
end
