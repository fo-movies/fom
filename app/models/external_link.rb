# == Schema Information
#
# Table name: external_links
#
#  id            :integer          not null, primary key
#  linkable_type :string
#  linkable_id   :integer
#  imdb_id       :string
#  tmdb_id       :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class ExternalLink < ApplicationRecord
  belongs_to :linkable, polymorphic: true
end
