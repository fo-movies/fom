# == Schema Information
#
# Table name: movies
#
#  id              :integer          not null, primary key
#  title           :string           not null
#  country_id      :integer          not null
#  language_id     :integer          not null
#  status_id       :integer
#  release_date    :date
#  summary         :text             not null
#  runtime         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  adult_movie_ind :boolean(0)
#  budget          :decimal(23,5)
#  revenue         :decimal(23,5)
#  original_title  :string
#  homepage        :string
#

class Movie < ApplicationRecord
  has_many :casts
  accepts_nested_attributes_for :casts
  has_many :media, as: :mediumable
  has_many :external_links, as: :linkable
end
