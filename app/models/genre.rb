# == Schema Information
#
# Table name: genres
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Genre < ApplicationRecord

  validates :name, presence: true, uniqueness: true
end
