# == Schema Information
#
# Table name: types
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Type < ApplicationRecord

  validates :name, presence: true, uniqueness: true
end
