# == Schema Information
#
# Table name: languages
#
#  id   :integer          not null, primary key
#  name :string           not null
#  iso  :string           not null
#

class Language < ApplicationRecord

  validates :name, :iso, presence: true, uniqueness: true
end
