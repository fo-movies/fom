# == Schema Information
#
# Table name: production_companies
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class ProductionCompany < ApplicationRecord

  validates :name, presence: true, uniqueness: true
end

