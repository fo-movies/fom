module Storage
  class Allocator

    def reference_data_store
      Storage::ReferenceDataStore.new
    end

    def movie_store
      Storage::MovieStore.new
    end

  end
end