module Storage
  class ReferenceDataStore
    def get_countries
      Country.all
    end

    def get_languages
      Language.all
    end
  end
end