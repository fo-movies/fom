module Builders
  class MovieBuilder < ObjectBuilder

    def new_movie
      Movie.new.tap do |movie|
       movie.casts.build(new_cast.attributes)
      end
    end

    def new_cast
      Cast.new
    end

  end
end