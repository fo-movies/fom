module Builders
  class ObjectBuilder
    attr_reader :user

    def initialize(user: nil)
      @user = user
    end

    def movie_builder
      Builders::MovieBuilder.new
    end
  end
end