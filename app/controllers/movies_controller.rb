class MoviesController < ApplicationController

  def new
    @movie = movie_service.new_movie
    assign_selection_lists
  end

  private

  def assign_selection_lists
    @countries = ref_data_service.get_countries
    @languages = ref_data_service.get_languages
  end

end