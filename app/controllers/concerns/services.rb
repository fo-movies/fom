module Services
  extend ActiveSupport::Concern
  included do

    def movie_service
      @movie_service ||= Services::MovieService.new(user: current_user)
    end

    def ref_data_service
      @ref_data_service ||= Services::ReferenceDataService.new(user: current_user)
    end

  end
end