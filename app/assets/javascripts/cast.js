movieCast = (function () {

    params = {
        castContainer: '#casts-container',
        castContainerDivs: '#casts-container > div'
    };

    function addCast() {
        var row = $(params.castContainer).find('.cast-template .row');
        if (row.length !== 1) {
            console.log('No row found to add cast')
        }
        row.clone().removeClass('hidden').appendTo($(params.castContainer));
        updateIdAndName();
    }

    function updateIdAndName() {
        $(params.castContainerDivs).not('.hidden').not('.cast-label').each(function (index, value) {
            $(this).find('.form-control').each(function () {
                var name = $(this).attr('name');
                var id = $(this).attr('id');
                $(this).attr('name', name.replace(name.match(/\[.*?\]/g)[1], "[" + index + "]"));
                $(this).attr('id', id.replace(id.split('_')[3], index));
            });
            $(this).find('button').each(function () {
                var id = $(this).attr('id');
                $(this).attr('id', id.replace(id.split('-')[2], index));
            });
            addDeleteButton(index);
        })
    }

    function addDeleteButton(index) {
        var maxSize = $(params.castContainerDivs).not('.hidden').not('.cast-label').length;
        $('#add-cast' + '-' + index).toggleClass('hidden', (index !== (maxSize - 1)));
        $('#remove-cast' + '-' + index).toggleClass('hidden', !(index !== (maxSize - 1)));

    }

    function deleteCast() {
        var row = $(event.currentTarget).closest('.row');
        if (row.length !== 1) {
            console.log('No row found to Remove cast')
        }
        row.remove();
        updateIdAndName();
    }


    return {
        addCast: addCast,
        deleteCast: deleteCast
    }

})();