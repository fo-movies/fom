require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'rspec/rails'
require 'webmock/rspec'
require 'simplecov'
SimpleCov.start 'rails'

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.include FactoryGirl::Syntax::Methods

  config.use_transactional_fixtures = true


  config.infer_spec_type_from_file_location!


  config.filter_rails_from_backtrace!


  # Drops Test DB for each run
  config.after :all do
    ActiveRecord::Base.subclasses.each(&:delete_all)
  end

  Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }
  Dir[Rails.root.join("spec/fixtures/storage/**/*.rb")].each { |f| require f }

  require 'database_cleaner'
  Dir["#{Rails.root}/app/models/**/*.rb"].each do |model|
    load model
  end
end
