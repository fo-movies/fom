# == Schema Information
#
# Table name: statuses
#
#  id   :integer          not null, primary key
#  name :string           not null
#

FactoryGirl.define do
  factory :status do
    name 'Released'
  end
end
