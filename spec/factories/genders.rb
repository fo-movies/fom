# == Schema Information
#
# Table name: genders
#
#  id   :integer          not null, primary key
#  name :string           not null
#

FactoryGirl.define do
  factory :gender do
    name 'Male'
  end
end
