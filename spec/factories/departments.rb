# == Schema Information
#
# Table name: departments
#
#  id   :integer          not null, primary key
#  name :string           not null
#

FactoryGirl.define do
  factory :department do
    name 'Producer'
  end
end
