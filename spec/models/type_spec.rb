# == Schema Information
#
# Table name: types
#
#  id   :integer          not null, primary key
#  name :string           not null
#

describe Type do

  subject(:type) { create :type }

  describe '# record validity' do
    it 'should not be valid - presence' do
      record = build(:type, name: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Name can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      create :type, name: 'TTT'
      record = build(:type, name: 'TTT')
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ['Name has already been taken']
    end

    it 'should be valid' do
      expect(build(:type, name: 'TTT1')).to be_valid
    end
  end

end
