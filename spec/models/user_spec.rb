# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  username               :string
#

require 'spec_helper'

describe User do

  let!(:user_attrs) { build(:user).attributes.merge!(password: 'password') }
  let!(:user) { User.new(user_attrs) }

  it 'should create user' do
    User.create!(build(:user).attributes.merge!(password: 'password'))
    expect(User.count).to eq(1)
  end

  describe 'username validations' do
    it 'should be valid if username is present' do
      expect(user).to be_valid
    end

    it 'should not be valid if username is nil' do
      user.username = nil
      expect(user).not_to be_valid
    end

    it 'should be not be valid if format is invalid' do
      user.username = 'User@*'
      expect(user).not_to be_valid
    end

    it 'should be valid if username is in valid format' do
      user.username = 'UserName1'
      expect(user).to be_valid
    end
  end

end
